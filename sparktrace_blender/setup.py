import os, sys
#Only seems to provide info from within Python distutils
#os.environ["DISTUTILS_DEBUG"] = "1"

##sys.path.insert(0, "C:/Program Files (x86)/Microsoft Visual Studio 12.0/VC/") 

from distutils.core import setup, Extension


INCLUDE_DIR = "C:/dev/Python35/include/"
LIBRARY_DIRS = [
    "C:/dev/Python35/libs/"#,
    #"C:/Program Files (x86)/Windows Kits/10/Lib/10.0.17134.0/um/x64/"
]


sparktrace_module = Extension(
    "sparktrace",
    include_dirs = [INCLUDE_DIR],
    libraries = ["OpenGL32"],
    library_dirs = LIBRARY_DIRS,
    sources = ["sparktrace.cpp"]
)

setup(
    name = "SparkTrace",
    version = "0.0.1",
    description = "SparkTrace is a physically-correct renderer.",
    author = "Ian Mallett",
    author_email = "ian" + chr(64) + "geometrian.com",
    url = "",
    long_description = "",
    ext_modules = [sparktrace_module]
)
