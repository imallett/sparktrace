#include <Python.h>

#ifdef _MSC_VER
	#include <Windows.h>
#endif
#include <gl/GL.h>

#include <cstdio>


//https://docs.python.org/3/extending/extending.html

//https://code.it4i.cz/blender/blender-embree3/blob/4cf7fc3b3a4d032f0c0db632a46d40806e906cf1/intern/cycles/blender/blender_python.cpp


static PyObject* create_func(PyObject* /*self*/, PyObject* args_py) {
	Py_RETURN_NONE;
}
static PyObject* reset_func (PyObject* /*self*/, PyObject* args_py) {
	PyObject *data_py, *scene_py;
	if (!PyArg_ParseTuple( args_py, "OO", &data_py,&scene_py )) return NULL;

	//BlenderSession *session = (BlenderSession*)PyLong_AsVoidPtr(pysession);

	/*PointerRNA dataptr;
	RNA_main_pointer_create((Main*)PyLong_AsVoidPtr(pydata), &dataptr);
	BL::BlendData b_data(dataptr);

	PointerRNA sceneptr;
	RNA_id_pointer_create((ID*)PyLong_AsVoidPtr(pyscene), &sceneptr);
	BL::Scene b_scene(sceneptr);

	python_thread_state_save(&session->python_thread_state);

	session->reset_session(b_data, b_scene);

	python_thread_state_restore(&session->python_thread_state);*/

	Py_RETURN_NONE;
}
static PyObject* free_func  (PyObject* /*self*/, PyObject* args_py) {
	Py_RETURN_NONE;
}

static PyObject* render_func(PyObject* /*self*/, PyObject* args_py) {
	//BlenderSession *session = (BlenderSession*)session;

	//python_thread_state_save(&session->python_thread_state);

	//session->render();

	//python_thread_state_restore(&session->python_thread_state);
	Py_RETURN_NONE;
}
static PyObject* draw_func  (PyObject* /*self*/, PyObject* args_py) {
	PyObject *v3d_py, *rv3d_py;
	if (!PyArg_ParseTuple( args_py, "OO", &v3d_py,&rv3d_py )) return NULL;

	//BlenderSession *session = (BlenderSession*)PyLong_AsVoidPtr(session_py);

	/*if (PyLong_AsVoidPtr(v3d_py)) {
		//3D view drawing
		int viewport[4];
		glGetIntegerv(GL_VIEWPORT, viewport);

		//session->draw(viewport[2], viewport[3]);
		float* framebuffer = reinterpret_cast<float*>(PyLong_AsVoidPtr(session_py));
		//for (int j=viewport[0];j<viewport[0]+viewport[2];++j) {
		//	for (int i=viewport[1];i<viewport[1]+viewport[3];++i) {
		for (size_t i=0;i<100;++i) {
			framebuffer[4*i  ] = 0.2f;
			framebuffer[4*i+1] = 0.6f;
			framebuffer[4*i+2] = 0.8f;
			framebuffer[4*i+3] = 1.0f;
		}
		//	}
		//}
	}*/

	Py_RETURN_NONE;
}

static PyObject* sync_func  (PyObject* /*self*/, PyObject* args_py) {
	//BlenderSession *session = (BlenderSession*)PyLong_AsVoidPtr(session_py);

	/*python_thread_state_save(&session->python_thread_state);

	session->synchronize();

	python_thread_state_restore(&session->python_thread_state);*/

	Py_RETURN_NONE;
}

static PyMethodDef sparktrace_functions[] = {
	{ "create", create_func, METH_VARARGS,""   },
	{ "reset",  reset_func,  METH_VARARGS,""   },
	{ "free",   free_func,   METH_VARARGS,""   },

	{ "render", render_func, METH_VARARGS,""   },
	{ "draw",   draw_func,   METH_VARARGS,""   },

	{ "sync",   sync_func,   METH_VARARGS,""   },

	{ NULL,     NULL,        0,           NULL }
};
static struct PyModuleDef sparktrace_module = {
	PyModuleDef_HEAD_INIT,
	"sparktrace",
	NULL, //documentation
	-1, //size of per-interpreter state of the module (-1 means state in global variables)
	sparktrace_functions
};

PyMODINIT_FUNC PyInit_sparktrace(void) {
	return PyModule_Create(&sparktrace_module);
}
