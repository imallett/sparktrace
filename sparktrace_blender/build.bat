SET MSSdk=1
SET DISTUTILS_USE_SDK=1
call "C:/Program Files (x86)/Microsoft Visual Studio 14.0/VC/vcvarsall.bat" amd64 8.1 -vcvars_ver=12.0

C:/dev/Python35/python35.exe setup.py build
::"C:\Program Files\Blender Foundation\Blender\2.79\python\bin\python.exe" setup.py build

::Note backslashes required.
echo F | xcopy /s "build\lib.win-amd64-3.5\sparktrace.cp35-win_amd64.pyd" "sparktrace.pyd" /y
echo F | xcopy /s "build\lib.win-amd64-3.5\sparktrace.cp35-win_amd64.pyd" "C:\Users\Ian Mallett\AppData\Roaming\Blender Foundation\Blender\2.79\scripts\addons\sparktrace.pyd" /y

rmdir /s /Q "build/"

set /p DUMMY=Hit ENTER to continue...
