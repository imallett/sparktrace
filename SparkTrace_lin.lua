solution("SparkTrace")
if _ACTION == "gmake" then
	location(".ides/make-lin/")
else
	location(".ides/")
end

language("C++")

plat = "lin"

configurations({"debug","release"})
if string.startswith(_ACTION,"vs") then
	--The ARM platform is not correctly implemented by Premake; it doesn't exist in the "architecture"
	--	command, and the Visual Studio generator appears to interpret that as an extra configuration.
	--	The other builders seem to work fine, however.
	platforms({"x32","x64"})
	--Use Intel compiler until such time as MSVC doesn't crash.  Comment this line if you don't have it.
	--premake.vstudio.toolset = "Intel C++ Compiler XE 16.0"
else
	platforms({"arm","x32","x64"})
end

if string.startswith(_ACTION,"vs") then
	vectorextensions("SSE2")
else --see above
	filter({"platforms:x32"}) buildoptions("-msse4.1")
	filter({"platforms:x64"}) buildoptions("-msse4.1")
	filter({"platforms:arm"}) buildoptions("-mfpu=neon")
	filter({"configurations:debug","platforms:arm"})
		buildoptions({ --Not yet implemented for GCC on ARM: "-fsanitize=memory", "-fsanitize=integer", "-fsanitize=leak"
			"-fsanitize=address",                  "-fsanitize-recover","-fno-omit-frame-pointer","-fno-common", "-fPIC",
			"-static-libasan",                   "-D_GLIBCXX_DEBUG","-D_GLIBCXX_DEBUG_PEDANTIC"
		})
		linkoptions({"-fsanitize=address","-static-libasan"})
	filter({"configurations:debug","platforms:x32"})
		--ASan is disabled on x86.  It wants to map to the same location as Valgrind, and there's apparently no way to change either address.
		buildoptions({ --Not yet implemented for GCC: "-fsanitize=memory", "-fsanitize=integer", "-fsanitize=leak" (no x86),
			--"-fsanitize=address","-fsanitize-recover",
			"-fno-omit-frame-pointer","-fno-common", "-fPIC",
			--"-static-libasan","-static-liblsan",
			"-D_GLIBCXX_DEBUG","-D_GLIBCXX_DEBUG_PEDANTIC"
		})
		--linkoptions({"-fsanitize=address","-static-libasan"})
	filter({"configurations:debug","platforms:x64"})
		buildoptions({ --Not yet implemented for GCC: "-fsanitize=memory", "-fsanitize=integer"
			"-fsanitize=address","-fsanitize=leak","-fsanitize-recover","-fno-omit-frame-pointer","-fno-common", "-fPIC",
			"-static-libasan","-static-liblsan", "-D_GLIBCXX_DEBUG","-D_GLIBCXX_DEBUG_PEDANTIC"
		})
		linkoptions({"-fsanitize=address","-static-libasan"})
end
filter({"platforms:x32"}) architecture("x32")
filter({"platforms:x64"}) architecture("x64")

filter({"configurations:debug"  }) defines({ "DEBUG"}) flags({"StaticRuntime"})
filter({"configurations:release"}) defines({"NDEBUG"}) flags({"StaticRuntime","OptimizeSpeed","NoBufferSecurityCheck"})

filter({})

symbols("On")

if string.startswith(_ACTION,"vs") then
	--https://software.intel.com/en-us/articles/limits1120-error-identifier-builtin-nanf-is-undefined
	--https://software.intel.com/en-us/forums/intel-c-compiler/topic/623368
	defines({"__builtin_huge_val()=HUGE_VAL","__builtin_huge_valf()=HUGE_VALF","__builtin_nan=nan","__builtin_nanf=nanf","__builtin_nans=nan","__builtin_nansf=nanf","__is_assignable=__is_trivially_assignable"})
	linkoptions({"/ignore:4221"})
end
defines({"ASH_SIMD_LEVEL=7"})

function file_exists(name)
	local f=io.open(name,"r")
	if f~=nil then io.close(f) return true else return false end
end
function add_project(name, dir_root, proj_kind)
	project(name)
	if file_exists(dir_root.."stdafx.cpp") then
		h = "stdafx.h"
		if not file_exists(dir_root..h) then h=h.."pp" else end
		if file_exists(dir_root..h) then
			if string.startswith(_ACTION,"vs") then
				pchheader(h)
				pchsource(dir_root.."stdafx.cpp")
				forceincludes({h})
			else
				pchheader(dir_root..h)
			end
		end
	end
	files({dir_root.."**.h",dir_root.."**.hpp",dir_root.."**.cpp",dir_root.."**.py",dir_root.."**.xml",dir_root.."**.txt",dir_root.."**.md"})
	kind(proj_kind)

	objdir(   ".build/"..plat.."_%{cfg.buildcfg}_%{cfg.platform}/%{cfg.buildtarget.basename}/")
	targetdir(".build/"..plat.."_%{cfg.buildcfg}_%{cfg.platform}/")

	debugdir("$(ProjectDir)..") --working dir for debugging programs (same as script dir)
end


defines({"_GLIBCXX_CONCEPT_CHECKS","UNICODE"})
--buildoptions({"-O3","-s"})
buildoptions({
	"-Wall",
	"-Wextra",
	"-Wdouble-promotion",
	"-Wformat-nonliteral",
	"-Wformat-security",
	"-Winit-self",
	"-Wmissing-include-dirs",
	"-Wswitch", "-Wswitch-default", --but no "-Wswitch-enum"
	"-Wuninitialized",
	"-Wmaybe-uninitialized",
	"-Wstrict-overflow=5",
	"-Wtrampolines",
	"-Wundef",
	"-Wcast-qual",
	"-Wcast-align",
	"-Wconversion",
	"-Wsign-conversion",
	"-Wlogical-op",
	"-Wnormalized=nfc",
	"-Wpacked",
	"-Wredundant-decls",
	"-Winvalid-pch",
	"-Wdisabled-optimization",

	"-Wctor-dtor-privacy",
	"-Wnoexcept",
	"-Wnon-virtual-dtor",
	--"-Woverloaded-virtual",
	"-Wreorder",
	"-Wsign-promo",
	"-Wstrict-null-sentinel",
	"-Wstrict-overflow=1",

	"-Wmissing-format-attribute",
	"-Wsuggest-override",

	"-Wzero-as-null-pointer-constant",
	"-Wold-style-cast",

	"-static-libstdc++",

	"-std=c++17",

	"-fexceptions"
})


add_project("libsparktrace","libsparktrace/","StaticLib")
	targetprefix("") targetextension(".a")
	includedirs({
		"",
		"../../C++/Libraries/libash/",
		"../../C++/Libraries/libchrono/",
		"../../C++/Libraries/libcrush/",
		"../../C++/Libraries/libgrfx/",
		"../../C++/Libraries/libib/",
		"../../C++/Libraries/libimgpp/",
		"../../C++/Libraries/libmastermind/",
		"../../C++/Libraries/libportcullis/",
		"../../C++/Libraries/libsi/",
		"../../C++/Libraries/libsyndicate/",
		"/usr/include/eigen3/",
		"/usr/include/freetype2/"
	})
	filter({})
add_project("sparktrace","sparktrace/","ConsoleApp")
	targetprefix("") targetextension(".out")
	includedirs({
		"",
		"../../C++/Libraries/libash/",
		"../../C++/Libraries/libchrono/",
		"../../C++/Libraries/libcrush/",
		"../../C++/Libraries/libgrfx/",
		"../../C++/Libraries/libib/",
		"../../C++/Libraries/libimgpp/",
		"../../C++/Libraries/libmastermind/",
		"../../C++/Libraries/libportcullis/",
		"../../C++/Libraries/libsi/",
		"../../C++/Libraries/libsyndicate/",
		"../../C++/SparkTrace/",
		"/usr/include/eigen3/",
		"/usr/include/freetype2/"
	})
	links({"libsparktrace"}) --ordering of builds within solution
	filter({"configurations:debug",  "platforms:arm"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libib/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libsi/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_debug_arm/",
			"../../C++/SparkTrace/.build/"..plat.."_debug_arm/",
			"/",
			"/usr/include/freetype2/"
		})
		links({
			":libsparktrace.a",
			":libgrfx.a",
			"tinyxml2",
			":libmastermind.a",
			":libsi.a",
			":libchrono.a",
			":libportcullis.a",
			":libimgpp.a",
			":libsyndicate.a",
			":libash.a",
			"freetype",
			"X11",
			"GL",
			":libcrush.a",
			"jpeg",
			"png",
			"tiff",
			"z",
			":libib.a",
			"pthread"
		})
	filter({"configurations:release","platforms:arm"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libib/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libsi/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_release_arm/",
			"../../C++/SparkTrace/.build/"..plat.."_release_arm/",
			"/",
			"/usr/include/freetype2/"
		})
		links({
			":libsparktrace.a",
			":libgrfx.a",
			"tinyxml2",
			":libmastermind.a",
			":libsi.a",
			":libchrono.a",
			":libportcullis.a",
			":libimgpp.a",
			":libsyndicate.a",
			":libash.a",
			"freetype",
			"X11",
			"GL",
			":libcrush.a",
			"jpeg",
			"png",
			"tiff",
			"z",
			":libib.a",
			"pthread"
		})
	filter({"configurations:debug",  "platforms:x32"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libib/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libsi/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_debug_x32/",
			"../../C++/SparkTrace/.build/"..plat.."_debug_x32/",
			"/",
			"/usr/include/freetype2/"
		})
		links({
			":libsparktrace.a",
			":libgrfx.a",
			"tinyxml2",
			":libmastermind.a",
			":libsi.a",
			":libchrono.a",
			":libportcullis.a",
			":libimgpp.a",
			":libsyndicate.a",
			":libash.a",
			"freetype",
			"X11",
			"GL",
			":libcrush.a",
			"jpeg",
			"png",
			"tiff",
			"z",
			":libib.a",
			"pthread"
		})
	filter({"configurations:release","platforms:x32"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libib/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libsi/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_release_x32/",
			"../../C++/SparkTrace/.build/"..plat.."_release_x32/",
			"/",
			"/usr/include/freetype2/"
		})
		links({
			":libsparktrace.a",
			":libgrfx.a",
			"tinyxml2",
			":libmastermind.a",
			":libsi.a",
			":libchrono.a",
			":libportcullis.a",
			":libimgpp.a",
			":libsyndicate.a",
			":libash.a",
			"freetype",
			"X11",
			"GL",
			":libcrush.a",
			"jpeg",
			"png",
			"tiff",
			"z",
			":libib.a",
			"pthread"
		})
	filter({"configurations:debug",  "platforms:x64"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libib/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libsi/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_debug_x64/",
			"../../C++/SparkTrace/.build/"..plat.."_debug_x64/",
			"/",
			"/usr/include/freetype2/"
		})
		links({
			":libsparktrace.a",
			":libgrfx.a",
			"tinyxml2",
			":libmastermind.a",
			":libsi.a",
			":libchrono.a",
			":libportcullis.a",
			":libimgpp.a",
			":libsyndicate.a",
			":libash.a",
			"freetype",
			"X11",
			"GL",
			":libcrush.a",
			"jpeg",
			"png",
			"tiff",
			"z",
			":libib.a",
			"pthread"
		})
	filter({"configurations:release","platforms:x64"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libib/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libsi/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_release_x64/",
			"../../C++/SparkTrace/.build/"..plat.."_release_x64/",
			"/",
			"/usr/include/freetype2/"
		})
		links({
			":libsparktrace.a",
			":libgrfx.a",
			"tinyxml2",
			":libmastermind.a",
			":libsi.a",
			":libchrono.a",
			":libportcullis.a",
			":libimgpp.a",
			":libsyndicate.a",
			":libash.a",
			"freetype",
			"X11",
			"GL",
			":libcrush.a",
			"jpeg",
			"png",
			"tiff",
			"z",
			":libib.a",
			"pthread"
		})
	filter({})
