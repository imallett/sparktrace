solution("SparkTrace")
if _ACTION == "gmake" then
	location(".ides/make-win/")
else
	location(".ides/")
end

language("C++")

plat = "win"

configurations({"debug","release"})
if string.startswith(_ACTION,"vs") then
	--The ARM platform is not correctly implemented by Premake; it doesn't exist in the "architecture"
	--	command, and the Visual Studio generator appears to interpret that as an extra configuration.
	--	The other builders seem to work fine, however.
	platforms({"x32","x64"})
	--Use Intel compiler until such time as MSVC doesn't crash.  Comment this line if you don't have it.
	--premake.vstudio.toolset = "Intel C++ Compiler XE 16.0"
else
	platforms({"arm","x32","x64"})
end

if string.startswith(_ACTION,"vs") then
	vectorextensions("SSE2")
else --see above
	filter({"platforms:x32"}) buildoptions("-msse4.1")
	filter({"platforms:x64"}) buildoptions("-msse4.1")
	filter({"platforms:arm"}) buildoptions("-mfpu=neon")
	filter({"configurations:debug","platforms:arm"})
		buildoptions({ --Not yet implemented for GCC on ARM: "-fsanitize=memory", "-fsanitize=integer", "-fsanitize=leak"
			"-fsanitize=address",                  "-fsanitize-recover","-fno-omit-frame-pointer","-fno-common", "-fPIC",
			"-static-libasan",                   "-D_GLIBCXX_DEBUG","-D_GLIBCXX_DEBUG_PEDANTIC"
		})
		linkoptions({"-fsanitize=address","-static-libasan"})
	filter({"configurations:debug","platforms:x32"})
		--ASan is disabled on x86.  It wants to map to the same location as Valgrind, and there's apparently no way to change either address.
		buildoptions({ --Not yet implemented for GCC: "-fsanitize=memory", "-fsanitize=integer", "-fsanitize=leak" (no x86),
			--"-fsanitize=address","-fsanitize-recover",
			"-fno-omit-frame-pointer","-fno-common", "-fPIC",
			--"-static-libasan","-static-liblsan",
			"-D_GLIBCXX_DEBUG","-D_GLIBCXX_DEBUG_PEDANTIC"
		})
		--linkoptions({"-fsanitize=address","-static-libasan"})
	filter({"configurations:debug","platforms:x64"})
		buildoptions({ --Not yet implemented for GCC: "-fsanitize=memory", "-fsanitize=integer"
			"-fsanitize=address","-fsanitize=leak","-fsanitize-recover","-fno-omit-frame-pointer","-fno-common", "-fPIC",
			"-static-libasan","-static-liblsan", "-D_GLIBCXX_DEBUG","-D_GLIBCXX_DEBUG_PEDANTIC"
		})
		linkoptions({"-fsanitize=address","-static-libasan"})
end
filter({"platforms:x32"}) architecture("x32")
filter({"platforms:x64"}) architecture("x64")

filter({"configurations:debug"  }) defines({ "DEBUG"}) flags({"StaticRuntime"})
filter({"configurations:release"}) defines({"NDEBUG"}) flags({"StaticRuntime","OptimizeSpeed","NoBufferSecurityCheck"})

filter({})

symbols("On")

if string.startswith(_ACTION,"vs") then
	--https://software.intel.com/en-us/articles/limits1120-error-identifier-builtin-nanf-is-undefined
	--https://software.intel.com/en-us/forums/intel-c-compiler/topic/623368
	defines({"__builtin_huge_val()=HUGE_VAL","__builtin_huge_valf()=HUGE_VALF","__builtin_nan=nan","__builtin_nanf=nanf","__builtin_nans=nan","__builtin_nansf=nanf","__is_assignable=__is_trivially_assignable"})
	linkoptions({"/ignore:4221"})
end
defines({"ASH_SIMD_LEVEL=7"})

function file_exists(name)
	local f=io.open(name,"r")
	if f~=nil then io.close(f) return true else return false end
end
function add_project(name, dir_root, proj_kind)
	project(name)
	if file_exists(dir_root.."stdafx.cpp") then
		h = "stdafx.h"
		if not file_exists(dir_root..h) then h=h.."pp" else end
		if file_exists(dir_root..h) then
			if string.startswith(_ACTION,"vs") then
				pchheader(h)
				pchsource(dir_root.."stdafx.cpp")
				forceincludes({h})
			else
				pchheader(dir_root..h)
			end
		end
	end
	files({dir_root.."**.h",dir_root.."**.hpp",dir_root.."**.cpp",dir_root.."**.py",dir_root.."**.xml",dir_root.."**.txt",dir_root.."**.md"})
	kind(proj_kind)

	objdir(   ".build/"..plat.."_%{cfg.buildcfg}_%{cfg.platform}/%{cfg.buildtarget.basename}/")
	targetdir(".build/"..plat.."_%{cfg.buildcfg}_%{cfg.platform}/")

	debugdir("$(ProjectDir)..") --working dir for debugging programs (same as script dir)
end


filter({"configurations:debug"  }) buildoptions({"/Zo"})
filter({"configurations:release"}) buildoptions({"/Zo"})
filter({})
defines({"_ALLOW_KEYWORD_MACROS","_CRT_SECURE_NO_WARNINGS","_SCL_SECURE_NO_WARNINGS"})
buildoptions({"/we4714","/we4715","/wd4661","/wd4800","/wd4007","/std:c++17"})
characterset("Unicode")
editandcontinue("Off")


add_project("libsparktrace","libsparktrace/","StaticLib")
	targetprefix("") targetextension(".lib")
	includedirs({
		"",
		"../../C++/Libraries/libash/",
		"../../C++/Libraries/libchrono/",
		"../../C++/Libraries/libcrush/",
		"../../C++/Libraries/libgrfx/",
		"../../C++/Libraries/libib/",
		"../../C++/Libraries/libimgpp/",
		"../../C++/Libraries/libmastermind/",
		"../../C++/Libraries/libportcullis/",
		"../../C++/Libraries/libsi/",
		"../../C++/Libraries/libsyndicate/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/eigen/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/freetype/include/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/glfw/include/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/libjpeg/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/libpng/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/libtiff/libtiff/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/tinyxml2/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/zlib/"
	})
	filter({})
add_project("sparktrace","sparktrace/","ConsoleApp")
	targetprefix("") targetextension(".exe")
	includedirs({
		"",
		"../../C++/Libraries/libash/",
		"../../C++/Libraries/libchrono/",
		"../../C++/Libraries/libcrush/",
		"../../C++/Libraries/libgrfx/",
		"../../C++/Libraries/libib/",
		"../../C++/Libraries/libimgpp/",
		"../../C++/Libraries/libmastermind/",
		"../../C++/Libraries/libportcullis/",
		"../../C++/Libraries/libsi/",
		"../../C++/Libraries/libsyndicate/",
		"../../C++/SparkTrace/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/eigen/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/freetype/include/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/glfw/include/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/libjpeg/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/libpng/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/libtiff/libtiff/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/tinyxml2/",
		"C:/Program Files (x86)/Windows Kits/10/Lib/user/zlib/"
	})
	links({"libsparktrace"}) --ordering of builds within solution
	filter({"configurations:debug",  "platforms:arm"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libib/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libsi/.build/"..plat.."_debug_arm/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_debug_arm/",
			"../../C++/SparkTrace/.build/"..plat.."_debug_arm/",
			"/"
		})
		links({
			"libsparktrace.lib",
			"libgrfx.lib",
			"libmastermind.lib",
			"libsi.lib",
			"libchrono.lib",
			"libportcullis.lib",
			"libimgpp.lib",
			"libsyndicate.lib",
			"libash.lib",
			"Ws2_32",
			"winmm",
			"Gdi32",
			"opengl32",
			"libcrush.lib",
			"libib.lib",
			"wsock32"
		})
	filter({"configurations:release","platforms:arm"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libib/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libsi/.build/"..plat.."_release_arm/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_release_arm/",
			"../../C++/SparkTrace/.build/"..plat.."_release_arm/",
			"/"
		})
		links({
			"libsparktrace.lib",
			"libgrfx.lib",
			"libmastermind.lib",
			"libsi.lib",
			"libchrono.lib",
			"libportcullis.lib",
			"libimgpp.lib",
			"libsyndicate.lib",
			"libash.lib",
			"Ws2_32",
			"winmm",
			"Gdi32",
			"opengl32",
			"libcrush.lib",
			"libib.lib",
			"wsock32"
		})
	filter({"configurations:debug",  "platforms:x32"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libib/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libsi/.build/"..plat.."_debug_x32/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_debug_x32/",
			"../../C++/SparkTrace/.build/"..plat.."_debug_x32/",
			"/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/freetype/builds/windows/vc2010/Win32/Debug Multithreaded/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/glfw/build/Win32/Debug/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libjpeg/build/Win32/Debug/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libpng/build/Win32/Debug Library/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libtiff/build/Win32/Debug/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/tinyxml2/tinyxml2/build/Win32/Debug-Lib/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/zlib/build/Win32/Debug/"
		})
		links({
			"libsparktrace.lib",
			"libgrfx.lib",
			"tinyxml2",
			"libmastermind.lib",
			"libsi.lib",
			"libchrono.lib",
			"libportcullis.lib",
			"libimgpp.lib",
			"libsyndicate.lib",
			"libash.lib",
			"freetype",
			"glfw3",
			"Ws2_32",
			"winmm",
			"Gdi32",
			"opengl32",
			"libcrush.lib",
			"libjpeg",
			"libpng",
			"tiff",
			"libz",
			"libib.lib",
			"wsock32"
		})
	filter({"configurations:release","platforms:x32"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libib/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libsi/.build/"..plat.."_release_x32/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_release_x32/",
			"../../C++/SparkTrace/.build/"..plat.."_release_x32/",
			"/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/freetype/builds/windows/vc2010/Win32/Release Multithreaded/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/glfw/build/Win32/RelWithDebInfo/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libjpeg/build/Win32/Release/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libpng/build/Win32/Release Library/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libtiff/build/Win32/RelWithDebInfo/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/tinyxml2/tinyxml2/build/Win32/Release-Lib/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/zlib/build/Win32/Release/"
		})
		links({
			"libsparktrace.lib",
			"libgrfx.lib",
			"tinyxml2",
			"libmastermind.lib",
			"libsi.lib",
			"libchrono.lib",
			"libportcullis.lib",
			"libimgpp.lib",
			"libsyndicate.lib",
			"libash.lib",
			"freetype",
			"glfw3",
			"Ws2_32",
			"winmm",
			"Gdi32",
			"opengl32",
			"libcrush.lib",
			"libjpeg",
			"libpng",
			"tiff",
			"libz",
			"libib.lib",
			"wsock32"
		})
	filter({"configurations:debug",  "platforms:x64"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libib/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libsi/.build/"..plat.."_debug_x64/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_debug_x64/",
			"../../C++/SparkTrace/.build/"..plat.."_debug_x64/",
			"/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/freetype/builds/windows/vc2010/x64/Debug Multithreaded/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/glfw/build/x64/Debug/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libjpeg/build/x64/Debug/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libpng/build/x64/Debug Library/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libtiff/build/x64/Debug/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/tinyxml2/tinyxml2/build/x64/Debug-Lib/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/zlib/build/x64/Debug/"
		})
		links({
			"libsparktrace.lib",
			"libgrfx.lib",
			"tinyxml2",
			"libmastermind.lib",
			"libsi.lib",
			"libchrono.lib",
			"libportcullis.lib",
			"libimgpp.lib",
			"libsyndicate.lib",
			"libash.lib",
			"freetype",
			"glfw3",
			"Ws2_32",
			"winmm",
			"Gdi32",
			"opengl32",
			"libcrush.lib",
			"libjpeg",
			"libpng",
			"tiff",
			"libz",
			"libib.lib",
			"wsock32"
		})
	filter({"configurations:release","platforms:x64"})
		libdirs({
			"../../C++/Libraries/libash/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libchrono/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libcrush/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libgrfx/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libib/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libimgpp/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libmastermind/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libportcullis/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libsi/.build/"..plat.."_release_x64/",
			"../../C++/Libraries/libsyndicate/.build/"..plat.."_release_x64/",
			"../../C++/SparkTrace/.build/"..plat.."_release_x64/",
			"/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/freetype/builds/windows/vc2010/x64/Release Multithreaded/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/glfw/build/x64/RelWithDebInfo/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libjpeg/build/x64/Release/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libpng/build/x64/Release Library/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/libtiff/build/x64/RelWithDebInfo/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/tinyxml2/tinyxml2/build/x64/Release-Lib/",
			"C:/Program Files (x86)/Windows Kits/10/Lib/user/zlib/build/x64/Release/"
		})
		links({
			"libsparktrace.lib",
			"libgrfx.lib",
			"tinyxml2",
			"libmastermind.lib",
			"libsi.lib",
			"libchrono.lib",
			"libportcullis.lib",
			"libimgpp.lib",
			"libsyndicate.lib",
			"libash.lib",
			"freetype",
			"glfw3",
			"Ws2_32",
			"winmm",
			"Gdi32",
			"opengl32",
			"libcrush.lib",
			"libjpeg",
			"libpng",
			"tiff",
			"libz",
			"libib.lib",
			"wsock32"
		})
	filter({})
